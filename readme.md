# kooky 

A simple cookiecutter template for personal projects. I was 
inspired from [Henk](https://github.com/hgrif/cookiecutter-ds-python/tree/master/%7B%7B%20cookiecutter.repo_name%20%7D%7D).

You should be able to immediately apply it via: 

```bash
> pip install -U cookiecutter
> cookiecutter https://gitlab.com/koaning/kooky.git
```

Note that if you'd like to make changes that you also need to 
change the `cookiecutter.json` file in the root dir. 