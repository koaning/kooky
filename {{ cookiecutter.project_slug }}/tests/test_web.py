from starlette.testclient import TestClient

from {{ cookiecutter.python_module_name }}.web import app

client = TestClient(app)

def test_root_endpoint():
    resp = client.get("/")
    assert resp.status_code == 200
